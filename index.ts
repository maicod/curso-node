import express from 'express';
import * as productos from './productos'

const app = express();
app.use(express.json());

app.get('/', function (request,response){

    response.send('Hello world');
})


app.get('/productos', function (request,response){

    response.send(productos.getStock());
})

app.post('/productos', function(request, response) {
    const body = request.body;
    const id = productos.getStock().length;
     productos.storeProductos(body,id)
    response.send("Agregaremos un producto a la lista");
})


app.put('/productos/:id', function(request, response){
    const body = request.body;
    const id= Number(request.params.id);
    productos.editProductos(body,id)
})

// app.delete('/productos/:id', function(request, response) {
//     const id= Number(request.params.id);
//     productos.deleteProductos(id);
//     response.send("Eliminaremos un producto de la lista");
// }) 

app.delete('/productos/:id', function(request, response)
{const id= Number(request.params.id);
    response.send(productos.deleteProductos(id));
})




app.listen(3000, function(){
    console.info('Servidor escuchando en http://localhost:3000');
})